pipeline {
    agent any

    parameters{
        string(
            name: "REGISTRY_URL", 
            defaultValue: "registry-itau.mastertech.com.br", 
            description: "Endereço do registry")
        string(
            name: "DOCKER_IMAGE", 
            defaultValue: "registry-itau.mastertech.com.br/itaujuntos-projeto-app-image", 
            description: "Imagem do Docker")
        string(
            name: "APPNAME", 
            defaultValue: "itaujuntos-projeto")
    }

    post {
        always {
            cleanWs()
        }
        failure {
            updateGitlabCommitStatus name: "build", state: "failed"
        }
        success {
            updateGitlabCommitStatus name: "build", state: "success"
        }
    }
    options {
        gitLabConnection("gitlab")
    }

    stages{
        stage("Gerar Pacote") {
            steps {
                echo "<--- Gerando pacote --->"
                sh "./mvnw package -DskipTests"
                echo "<--- Pacote finalizado com sucesso! --->"
            }
        }

        stage("Deploy Desenvolvimento"){
            when{branch "develop"}
            stages{
                stage("Push Docker Image"){
                    steps{
                        script {
                            docker.withRegistry("https://${params.REGISTRY_URL}","registry_credential"){
                                def customImage=docker.build("${params.DOCKER_IMAGE}-dev")
                                customImage.push("${env.BUILD_ID}")
                                customImage.push("latest")
                                echo "<--- Imagem ${params.DOCKER_IMAGE}-dev:${env.BUILD_ID} gerada com sucesso!--->"
                            }
                        }
                    }
                }
                stage("Rollout Kubernetes"){
                    steps{
                        echo "<--- Iniciando Rollout Deployment em desenvolvimento --->"
                        sh "kubectl rollout restart -n ${params.APPNAME}-nsp-dev deployment/${params.APPNAME}-app-deployment-dev"
                        echo "<--- Rollout de desenvolvimento finalizado com sucesso! --->"
                    }
                }
            }
        }

        stage("Deploy Produção"){
            when{branch "master"}
            stages{
                stage("Push Docker Image"){
                    steps{
                        script {
                            docker.withRegistry("https://${params.REGISTRY_URL}","registry_credential"){
                                def customImage=docker.build("${params.DOCKER_IMAGE}")
                                customImage.push("${env.BUILD_ID}")
                                customImage.push("latest")
                                echo "<--- Imagem ${params.DOCKER_IMAGE}:${env.BUILD_ID} gerada com sucesso!--->"
                            }
                        }
                    }
                }
                stage("Rollout Kubernetes"){
                    steps{
                        echo "<--- Iniciando Rollout Deployment em produção --->"
                        sh "kubectl rollout restart -n ${params.APPNAME}-nsp deployment/${params.APPNAME}-app-deployment"
                        echo "<--- Rollout de produção finalizado com sucesso! --->"
                    }
                }
            }
        }
    }
}
