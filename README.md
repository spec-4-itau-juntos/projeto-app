 
# Cadastro de Projetos

Manutenção de projetos sociais associados a uma organização.

## Funcionalidades Projetos

- CRUD de Projetos Sociais

### Contratos Projetos

- GET /v1/organizacao/{id}/projeto - Retorna a lista de projetos da organização
- GET /v1/organizacao/{id}/projeto?tipo=1 - (OPCIONAL) Retorna a lista de projetos da organização e pelo tipo.
**Status Codes**:
  - 200: OK
  - 204: Projetos não encontrados para a organização
  - 404: Organização não encontrada
    **Response**:

    ```Json
    [
        {  
            "id": 1,
            "nome": "República Africana",
            "descricao": "Conflitos reiniciados e em pleno desenvolvimento em grande parte da República Centro- Africana (RCA) produziram, em 2018, cenas de extrema violência contra pessoas que ainda sofriam do trauma da guerra civil que destruiu o país.",
            "id_organizacao": 1,
            "tipo": 1
        }
    ]
    ```

- GET /v1/organizacao/{id}/projeto/{id_projeto} - Retorna o projeto da organização
**Status Codes**:
  - 200: OK
  - 404: Projeto não encontrado para a organização
  - 404: Organização não encontrada
    **Response**:

    ```Json
    {  
        "id": 1,
        "nome": "República Africana",
        "descricao": "Conflitos reiniciados e em pleno desenvolvimento em grande parte da República Centro- Africana (RCA) produziram, em 2018, cenas de extrema violência contra pessoas que ainda sofriam do trauma da guerra civil que destruiu o país.",
        "id_organizacao": 1,
        "tipo": 1
    }
    ```

- POST /v1/organizacao/{id}/projeto - Adiciona um novo projeto à organização
**Status Codes**:
  - 201: Projeto criado
  - 400: Bad Request

    **Request**:

    ```Json
    {
        "nome": "República Centro Africana",
        "descricao": "Conflitos reiniciados e em pleno desenvolvimento em grande parte da República Centro- Africana (RCA) produziram, em 2018, cenas de extrema violência contra pessoas que ainda sofriam do trauma da guerra civil que destruiu o país.",
        "tipo": 1
    }
    ```

    **Response**:

    ```Json
    {  
        "id": 1,
        "nome": "República Centro Africana",
        "descricao": "Conflitos reiniciados e em pleno desenvolvimento em grande parte da República Centro- Africana (RCA) produziram, em 2018, cenas de extrema violência contra pessoas que ainda sofriam do trauma da guerra civil que destruiu o país.",
        "id_organizacao": 1,
        "tipo": 1
    }
    ```

- DELETE /v1/organizacao/{id}/projeto/{id_projeto} - Deleta o projeto pelo id
**Status Codes**:
  - 204: Projeto deletado
  - 404: Projeto não encontrado para a organização
  - 404: Organização não encontrada
- PATCH /v1/organizacao/{id}/projeto/{id_projeto} - Atualiza o nome do projeto e a descrição
**Status Codes**:
  - 200: Projeto alterado
  - 404: Projeto não encontrado para a organização
  - 404: Organização não encontrada
  - 400: Bad Request

    **Request**:

    ```Json
    {
        "nome": "República Africana",
    }
    ```

    **Response**:

    ```Json
    {  
        "id": 1,
        "nome": "República Africana",
        "descricao": "Conflitos reiniciados e em pleno desenvolvimento em grande parte da República Centro- Africana (RCA) produziram, em 2018, cenas de extrema violência contra pessoas que ainda sofriam do trauma da guerra civil que destruiu o país.",
        "id_organizacao": 1,
        "tipo": 1
    }
    ```

- GET /v1/tipo - Retorna a lista de tipos possíves de projetos
**Status Codes**
  - 200: OK
    **Response**:

    ```Json
    [
        {  
            "id": 1,
            "nome": "Ajuda a animais abandonados",
            "descricao": "Projetos que tenham como objetivo o bem-estar animal."
        },
        {  
            "id": 2,
            "nome": "Apoio a mulheres vítimas de violência",
            "descricao": "Projetos com o objetivo de prevenção e erradicação de todas as formas de violência contra as mulheres."
        }
    ]
    ```

- GET /v1/tipo/{id} - Retorna o tipo pelo id
**Status Codes**
  - 200: OK
    **Response**:

    ```Json
    {  
        "id": 1,
        "nome": "Ajuda a animais abandonados",
        "descricao": "Projetos que tenham como objetivo o bem-estar animal."
    }
    ```

- POST /v1/tipo - Adiciona um novo tipo
**Status Codes**
  - 201: Novo tipo criado
  **Request**:

    ```Json
    {  
        "nome": "Ajuda a animais abandonados",
        "descricao": "Projetos que tenham como objetivo o bem-estar animal."
    }
    ```

    **Response**:

    ```Json
    {  
        "id": 1,
        "nome": "Ajuda a animais abandonados",
        "descricao": "Projetos que tenham como objetivo o bem-estar animal."
    }
    ```

- PATCH /v1/tipo/{id} - Edita um tipo pelo id informado
**Status Codes**
  - 200: OK
  - 404: Tipo não encontrado.
  **Request**:

    ```Json
    {  
        "nome": "Ajuda a animais"
    }
    ```

    **Response**:

    ```Json
    {  
        "id": 1,
        "nome": "Ajuda a animais",
        "descricao": "Projetos que tenham como objetivo o bem-estar animal."
    }
    ```

#### Exemplos de Projetos

- República Centro Africana
- Sudão do Sul
- Bangladesh

#### Exemplos de Tipos de Projetos

- Combate ao abuso infantil e apoio às vítimas
- Educação de crianças e jovens
- Crianças desaparecidas
- Apoio a mulheres vítimas de violência
- Empoderamento feminino
- Combate à desigualdade racial
- Apoio a pessoas em situação de rua
- Combate à pobreza
- Inclusão de refugiados
- Inclusão de pessoas com deficiência
- Apoio a dependentes químicos
- Acolhimento e melhora da qualidade de vida de idosos
- Defesa dos direitos indígenas
- Preservação do meio ambiente
- Ajuda a animais abandonados

##### Tabelas Projetos

Projeto
| id   | nome   | descricao | id_organizacao | tipo    |
| ---- | ------ | --------- | -------------- | ------- |
| long | string | string    | long           | id_tipo |

Tipo
| id   | nome   | descricao |
| ---- | ------ | --------- |
| long | string | string    |