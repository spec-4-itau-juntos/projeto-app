FROM openjdk:8-alpine
COPY target/projeto-app-*.jar itaujuntos-projeto-app.jar
CMD ["java", "-jar", "itaujuntos-projeto-app.jar"]