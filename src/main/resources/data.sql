DROP TABLE IF EXISTS tipo CASCADE;

CREATE TABLE tipo
(
    id        INT AUTO_INCREMENT PRIMARY KEY,
    nome      VARCHAR(150) NOT NULL,
    descricao VARCHAR(1000) NOT NULL
);

DROP TABLE IF EXISTS projeto CASCADE;

CREATE TABLE projeto
(
    id             INT AUTO_INCREMENT PRIMARY KEY,
    nome           VARCHAR(150) NOT NULL,
    descricao      VARCHAR(1000) NOT NULL,
    id_organizacao INT          NOT NULL,
    tipo           INT          NOT NULL,
    foreign key (tipo) references tipo (id)
);

INSERT INTO tipo (nome, descricao) VALUES
('Combate ao abuso infantil e apoio às vítimas', 'Combate ao abuso infantil e apoio às vítimas'),
('Educação de crianças e jovens', 'Educação de crianças e jovens'),
('Crianças desaparecidas', 'Crianças desaparecidas'),
('Apoio a mulheres vítimas de violência', 'Apoio a mulheres vítimas de violência'),
('Empoderamento feminino', 'Empoderamento feminino'),
('Combate à desigualdade racial', 'Combate à desigualdade racial'),
('Apoio a pessoas em situação de rua', 'Apoio a pessoas em situação de rua'),
('Combate à pobreza', 'Combate à pobreza'),
('Inclusão de refugiados', 'Inclusão de refugiados'),
('Inclusão de pessoas com deficiência', 'Inclusão de pessoas com deficiência'),
('Apoio a dependentes químicos', 'Apoio a dependentes químicos'),
('Acolhimento e melhora da qualidade de vida de idosos', 'Acolhimento e melhora da qualidade de vida de idosos'),
('Defesa dos direitos indígenas', 'Defesa dos direitos indígenas'),
('Preservação do meio ambiente', 'Preservação do meio ambiente'),
('Ajuda a animais abandonados', 'Ajuda a animais abandonados');

INSERT INTO projeto (nome, descricao, id_organizacao, tipo) VALUES
('República Centro Africana', 'Conflitos reiniciados e em pleno desenvolvimento em grande parte da República Centro- Africana (RCA) produziram, em 2018, cenas de extrema violência contra pessoas que ainda sofriam do trauma da guerra civil que destruiu o país.', 1, 1);