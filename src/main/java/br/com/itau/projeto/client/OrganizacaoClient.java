package br.com.itau.projeto.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "itaujuntos-organizacao")
public interface OrganizacaoClient {

    @GetMapping("/v1/organizacao/{id}")
    OrganizacaoResponse getOrganizacao(@PathVariable Long id);
}
