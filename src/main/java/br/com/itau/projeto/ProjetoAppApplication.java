package br.com.itau.projeto;

import br.com.itau.util.handler.GlobalExceptionHandler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(GlobalExceptionHandler.class)
@EnableDiscoveryClient
@EnableFeignClients
public class ProjetoAppApplication {
    public static void main(String[] args) {
        SpringApplication.run(ProjetoAppApplication.class, args);
    }
}
