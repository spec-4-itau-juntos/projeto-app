package br.com.itau.projeto.entity.builder;

import br.com.itau.projeto.entity.ProjetoEntity;

public final class ProjetoEntityBuilder {
    private String nome;
    private String descricao;
    private Long idOrganizacao;
    private Long tipo;

    private ProjetoEntityBuilder() {
    }

    public static ProjetoEntityBuilder aProjetoEntity() {
        return new ProjetoEntityBuilder();
    }

    public ProjetoEntityBuilder nome(String nome) {
        this.nome = nome;
        return this;
    }

    public ProjetoEntityBuilder descricao(String descricao) {
        this.descricao = descricao;
        return this;
    }

    public ProjetoEntityBuilder idOrganizacao(Long idOrganizacao) {
        this.idOrganizacao = idOrganizacao;
        return this;
    }

    public ProjetoEntityBuilder tipo(Long tipo) {
        this.tipo = tipo;
        return this;
    }

    public ProjetoEntity build() {
        return new ProjetoEntity(nome, descricao, idOrganizacao, tipo);
    }
}
