package br.com.itau.projeto.entity;

import br.com.itau.projeto.entity.builder.TipoEntityBuilder;

import javax.persistence.*;

@Entity
@Table(name = "tipo")
public class TipoEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String nome;
    @Column
    private String descricao;

    public TipoEntity() {
    }

    public TipoEntity(String nome, String descricao) {
        this.nome = nome;
        this.descricao = descricao;
    }

    public static TipoEntityBuilder builder(){
        return TipoEntityBuilder.aTipoEntity();
    }

    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
