package br.com.itau.projeto.entity;

import br.com.itau.projeto.entity.builder.ProjetoEntityBuilder;

import javax.persistence.*;

@Entity
@Table(name = "projeto")
public class ProjetoEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String nome;
    @Column
    private String descricao;
    @Column(name = "id_organizacao")
    private Long idOrganizacao;
    @Column
    private Long tipo;

    public ProjetoEntity() {
    }

    public ProjetoEntity(String nome, String descricao, Long idOrganizacao, Long tipo) {
        this.nome = nome;
        this.descricao = descricao;
        this.idOrganizacao = idOrganizacao;
        this.tipo = tipo;
    }

    public static ProjetoEntityBuilder builder(){
        return ProjetoEntityBuilder.aProjetoEntity();
    }

    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public Long getIdOrganizacao() {
        return idOrganizacao;
    }

    public Long getTipo() {
        return tipo;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public void setTipo(Long tipo) {
        this.tipo = tipo;
    }
}
