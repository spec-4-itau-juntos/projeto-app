package br.com.itau.projeto.entity.builder;

import br.com.itau.projeto.entity.TipoEntity;

public final class TipoEntityBuilder {
    private String nome;
    private String descricao;

    private TipoEntityBuilder() {
    }

    public static TipoEntityBuilder aTipoEntity() {
        return new TipoEntityBuilder();
    }

    public TipoEntityBuilder nome(String nome) {
        this.nome = nome;
        return this;
    }

    public TipoEntityBuilder descricao(String descricao) {
        this.descricao = descricao;
        return this;
    }

    public TipoEntity build() {
        return new TipoEntity(nome, descricao);
    }
}
