package br.com.itau.projeto.dto;

public class ProjetoEdit {
    private String nome;
    private String descricao;
    private Long tipo;

    public ProjetoEdit(String nome, String descricao, Long tipo) {
        this.nome = nome;
        this.descricao = descricao;
        this.tipo = tipo;
    }

    public String getNome() {
        return nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public Long getTipo() {
        return tipo;
    }
}
