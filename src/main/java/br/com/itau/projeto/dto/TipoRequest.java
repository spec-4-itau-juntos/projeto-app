package br.com.itau.projeto.dto;

import br.com.itau.projeto.dto.builder.TipoRequestBuilder;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class TipoRequest {
    private Long id;
    @NotNull(message = "O nome do tipo não pode ser nulo.")
    @NotEmpty(message = "O nome do tipo não pode ser vazio.")
    private String nome;
    @NotNull(message = "A descrição do tipo não pode ser nula.")
    @NotEmpty(message = "A descrição do tipo não pode ser vazia.")
    private String descricao;

    public TipoRequest(Long id, String nome, String descricao) {
        this.id = id;
        this.nome = nome;
        this.descricao = descricao;
    }

    public static TipoRequestBuilder builder(){
        return TipoRequestBuilder.aTipoRequest();
    }

    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String getDescricao() {
        return descricao;
    }
}
