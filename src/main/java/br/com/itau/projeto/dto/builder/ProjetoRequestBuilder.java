package br.com.itau.projeto.dto.builder;

import br.com.itau.projeto.dto.ProjetoRequest;

public final class ProjetoRequestBuilder {
    private Long id;
    private String nome;
    private String descricao;
    private Long idOrganizacao;
    private Long tipo;

    private ProjetoRequestBuilder() {
    }

    public static ProjetoRequestBuilder aProjetoRequest() {
        return new ProjetoRequestBuilder();
    }

    public ProjetoRequestBuilder id(Long id) {
        this.id = id;
        return this;
    }

    public ProjetoRequestBuilder nome(String nome) {
        this.nome = nome;
        return this;
    }

    public ProjetoRequestBuilder descricao(String descricao) {
        this.descricao = descricao;
        return this;
    }

    public ProjetoRequestBuilder idOrganizacao(Long idOrganizacao) {
        this.idOrganizacao = idOrganizacao;
        return this;
    }

    public ProjetoRequestBuilder tipo(Long tipo) {
        this.tipo = tipo;
        return this;
    }

    public ProjetoRequest build() {
        return new ProjetoRequest(id, nome, descricao, idOrganizacao, tipo);
    }
}
