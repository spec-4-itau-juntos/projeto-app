package br.com.itau.projeto.dto.builder;

import br.com.itau.projeto.dto.TipoRequest;

public final class TipoRequestBuilder {
    private Long id;
    private String nome;
    private String descricao;

    private TipoRequestBuilder() {
    }

    public static TipoRequestBuilder aTipoRequest() {
        return new TipoRequestBuilder();
    }

    public TipoRequestBuilder id(Long id) {
        this.id = id;
        return this;
    }

    public TipoRequestBuilder nome(String nome) {
        this.nome = nome;
        return this;
    }

    public TipoRequestBuilder descricao(String descricao) {
        this.descricao = descricao;
        return this;
    }

    public TipoRequest build() {
        return new TipoRequest(id, nome, descricao);
    }
}
