package br.com.itau.projeto.dto;

import br.com.itau.projeto.dto.builder.ProjetoRequestBuilder;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ProjetoRequest {
    private Long id;
    @NotNull(message = "O nome do projeto não pode ser nulo.")
    @NotEmpty(message = "O nome do projeto não pode ser vazio.")
    private String nome;
    @NotNull(message = "A descrição do projeto não pode ser nula.")
    @NotEmpty(message = "A descrição do projeto não pode ser vazia.")
    private String descricao;
    @JsonProperty("id_organizacao")
    @NotNull(message = "O id da organização não pode ser nulo.")
    @Min(value = 1, message = "O id da organização deve ser um número positivo.")
    private Long idOrganizacao;
    @NotNull(message = "O id do tipo não pode ser nulo.")
    @Min(value = 1, message = "O id do tipo deve ser um número positivo.")
    private Long tipo;

    public ProjetoRequest(Long id, String nome, String descricao, Long idOrganizacao, Long tipo) {
        this.id = id;
        this.nome = nome;
        this.descricao = descricao;
        this.idOrganizacao = idOrganizacao;
        this.tipo = tipo;
    }

    public static ProjetoRequestBuilder builder(){
        return ProjetoRequestBuilder.aProjetoRequest();
    }

    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public Long getIdOrganizacao() {
        return idOrganizacao;
    }

    public Long getTipo() {
        return tipo;
    }
}
