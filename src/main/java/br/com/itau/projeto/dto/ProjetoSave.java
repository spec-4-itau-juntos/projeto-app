package br.com.itau.projeto.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class ProjetoSave {
    @NotNull(message = "O nome do projeto não pode ser nulo.")
    @NotEmpty(message = "O nome do projeto não pode ser vazio.")
    private String nome;
    @NotNull(message = "A descrição do projeto não pode ser nula.")
    @NotEmpty(message = "A descrição do projeto não pode ser vazia.")
    private String descricao;
    @NotNull(message = "O id do tipo não pode ser nulo.")
    @Min(value = 1, message = "O id do tipo deve ser um número positivo.")
    private Long tipo;

    public ProjetoSave(String nome, String descricao, Long tipo) {
        this.nome = nome;
        this.descricao = descricao;
        this.tipo = tipo;
    }

    public String getNome() {
        return nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public Long getTipo() {
        return tipo;
    }
}
