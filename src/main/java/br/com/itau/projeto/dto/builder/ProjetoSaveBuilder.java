package br.com.itau.projeto.dto.builder;

import br.com.itau.projeto.dto.ProjetoSave;

public final class ProjetoSaveBuilder {
    private String nome;
    private String descricao;
    private Long tipo;

    private ProjetoSaveBuilder() {
    }

    public static ProjetoSaveBuilder aProjetoSave() {
        return new ProjetoSaveBuilder();
    }

    public ProjetoSaveBuilder nome(String nome) {
        this.nome = nome;
        return this;
    }

    public ProjetoSaveBuilder descricao(String descricao) {
        this.descricao = descricao;
        return this;
    }

    public ProjetoSaveBuilder tipo(Long tipo) {
        this.tipo = tipo;
        return this;
    }

    public ProjetoSave build() {
        return new ProjetoSave(nome, descricao, tipo);
    }
}
