package br.com.itau.projeto.mapper;

import br.com.itau.projeto.dto.ProjetoEdit;
import br.com.itau.projeto.dto.ProjetoRequest;
import br.com.itau.projeto.dto.ProjetoSave;
import br.com.itau.projeto.dto.TipoRequest;
import br.com.itau.projeto.entity.ProjetoEntity;
import br.com.itau.projeto.entity.TipoEntity;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DataMapper {
    DataMapper INSTANCE = Mappers.getMapper(DataMapper.class);

    ProjetoRequest projetoEntityParaProjetoRequest(ProjetoEntity projetoEntity);

    List<ProjetoRequest> projetoEntityParaProjetoRequest(List<ProjetoEntity> projetoEntity);

    ProjetoEntity projetoSaveParaProjetoEntity(ProjetoSave projetoSave);

    ProjetoEntity projetoEditParaProjetoEntity(ProjetoEdit projetoSave);

    TipoEntity tipoRequestParaTipoEntity(TipoRequest tipoRequest);

    TipoRequest tipoEntityParaTipoRequest(TipoEntity tipoEntity);

    List<TipoRequest> tipoEntityParaTipoRequest(List<TipoEntity> tipoEntity);
}
