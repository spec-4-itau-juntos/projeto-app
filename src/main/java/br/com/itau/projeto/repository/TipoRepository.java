package br.com.itau.projeto.repository;

import br.com.itau.projeto.entity.TipoEntity;
import org.springframework.data.repository.CrudRepository;

public interface TipoRepository extends CrudRepository<TipoEntity, Long> {}