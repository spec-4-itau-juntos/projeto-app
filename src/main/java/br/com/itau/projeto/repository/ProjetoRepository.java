package br.com.itau.projeto.repository;

import br.com.itau.projeto.entity.ProjetoEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProjetoRepository extends CrudRepository<ProjetoEntity, Long> {
    List<ProjetoEntity> findByIdOrganizacao(Long idOrganizacao);

    List<ProjetoEntity> findByIdOrganizacaoAndTipo(Long idOrganizacao, Long tipo);
}
