package br.com.itau.projeto.controller;

import br.com.itau.projeto.dto.TipoRequest;
import br.com.itau.projeto.entity.TipoEntity;
import br.com.itau.projeto.mapper.DataMapper;
import br.com.itau.projeto.service.TipoService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.net.URI;
import java.util.List;

@RestController
@Validated
@RequestMapping("/v1/tipo")
public class TipoController {

    private final TipoService tipoService;

    public TipoController(TipoService tipoService) {
        this.tipoService = tipoService;
    }

    @GetMapping
    public ResponseEntity getTipos() {
        List<TipoEntity> tiposList = tipoService.getAllTipos();
        List<TipoRequest> tipoRequestList = DataMapper.INSTANCE.tipoEntityParaTipoRequest(tiposList);
        return ResponseEntity.ok(tipoRequestList);
    }

    @GetMapping("/{id}")
    public ResponseEntity getTipo(
            @Valid
            @NotNull(message = "O id não pode ser nulo.")
            @Min(value = 1, message = "O id deve ser um número positivo.")
            @PathVariable("id") Long id) {
        TipoEntity tipoEntity = tipoService.getById(id);
        TipoRequest tipoRequest = DataMapper.INSTANCE.tipoEntityParaTipoRequest(tipoEntity);
        return ResponseEntity.ok(tipoRequest);
    }

    @PostMapping
    public ResponseEntity saveTipo(@RequestBody @Valid TipoRequest tipoRequest) {
        TipoEntity tipoEntity = DataMapper.INSTANCE.tipoRequestParaTipoEntity(tipoRequest);
        TipoEntity tipoEntitySaved = tipoService.saveTipo(tipoEntity);
        TipoRequest tipoRequestSaved = DataMapper.INSTANCE.tipoEntityParaTipoRequest(tipoEntitySaved);
        return ResponseEntity.created(URI.create("")).body(tipoRequestSaved);
    }

    @PatchMapping("/{id}")
    public ResponseEntity editTipo(
            @Valid
            @NotNull(message = "O id não pode ser nulo.")
            @Min(value = 1, message = "O id deve ser um número positivo.")
            @PathVariable("id") Long id,
            @RequestBody @Valid TipoRequest tipoRequest) {
        TipoEntity tipoEntity = DataMapper.INSTANCE.tipoRequestParaTipoEntity(tipoRequest);
        TipoEntity tipoEntityEdited = tipoService.editTipo(id, tipoEntity);
        TipoRequest tipoRequestEdited = DataMapper.INSTANCE.tipoEntityParaTipoRequest(tipoEntityEdited);
        return ResponseEntity.ok().body(tipoRequestEdited);
    }
}
