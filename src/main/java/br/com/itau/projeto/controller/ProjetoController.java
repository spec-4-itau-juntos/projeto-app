package br.com.itau.projeto.controller;

import br.com.itau.projeto.dto.ProjetoEdit;
import br.com.itau.projeto.dto.ProjetoRequest;
import br.com.itau.projeto.dto.ProjetoSave;
import br.com.itau.projeto.entity.ProjetoEntity;
import br.com.itau.projeto.mapper.DataMapper;
import br.com.itau.projeto.service.ProjetoService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.net.URI;
import java.util.List;

@RestController
@Validated
@RequestMapping("/v1/organizacao")
public class ProjetoController {

    private final ProjetoService projetoService;

    public ProjetoController(ProjetoService projetoService) {
        this.projetoService = projetoService;
    }

    @GetMapping("/{id_organizacao}/projeto")
    public ResponseEntity getProjetosPorOrganizacao(
            @Valid
            @NotNull(message = "O id da organização não pode ser nulo.")
            @Min(value = 1, message = "O id da organização deve ser um número positivo.")
            @PathVariable("id_organizacao") Long idOrganizacao,
            @RequestParam(value = "tipo", required = false) Long tipo) {
        List<ProjetoEntity> projetoList;
        if(tipo == null) {
            projetoList = projetoService.findByIdOrganizacao(idOrganizacao);
        } else {
            projetoList = projetoService.findByIdOrganizacaoAndTipo(idOrganizacao, tipo);
        }

        List<ProjetoRequest> projetoRequests = DataMapper.INSTANCE.projetoEntityParaProjetoRequest(projetoList);
        return projetoRequests.isEmpty() ? ResponseEntity.noContent().build() : ResponseEntity.ok(projetoRequests);
    }

    @GetMapping("/{id_organizacao}/projeto/{id_projeto}")
    public ResponseEntity getOrganizacao(
            @Valid
            @NotNull(message = "O id da organização não pode ser nulo.")
            @Min(value = 1, message = "O id da organização deve ser um número positivo.")
            @PathVariable("id_organizacao") Long idOrganizacao,
            @Valid
            @NotNull(message = "O id do projeto não pode ser nulo.")
            @Min(value = 1, message = "O id do projeto deve ser um número positivo.")
            @PathVariable("id_projeto") Long idProjeto) {
        ProjetoEntity projetoEntity = projetoService.findById(idOrganizacao, idProjeto);
        ProjetoRequest projetoRequest = DataMapper.INSTANCE.projetoEntityParaProjetoRequest(projetoEntity);
        return ResponseEntity.ok(projetoRequest);
    }

    @PostMapping("/{id_organizacao}/projeto")
    public ResponseEntity saveOrganizacao(
            @Valid
            @NotNull(message = "O id da organização não pode ser nulo.")
            @Min(value = 1, message = "O id da organização deve ser um número positivo.")
            @PathVariable("id_organizacao") Long idOrganizacao,
            @RequestBody @Valid ProjetoSave projetoSave) {
        ProjetoEntity projetoEntity = DataMapper.INSTANCE.projetoSaveParaProjetoEntity(projetoSave);
        ProjetoEntity projetoEntitySaved = projetoService.save(idOrganizacao, projetoEntity);
        ProjetoRequest projetoRequestDb = DataMapper.INSTANCE.projetoEntityParaProjetoRequest(projetoEntitySaved);
        return ResponseEntity.created(URI.create("")).body(projetoRequestDb);
    }

    @PatchMapping("/{id_organizacao}/projeto/{id_projeto}")
    public ResponseEntity editOrganizacao(
            @Valid
            @NotNull(message = "O id da organização não pode ser nulo.")
            @Min(value = 1, message = "O id da organização deve ser um número positivo.")
            @PathVariable("id_organizacao") Long idOrganizacao,
            @Valid
            @NotNull(message = "O id do projeto não pode ser nulo.")
            @Min(value = 1, message = "O id do projeto deve ser um número positivo.")
            @PathVariable("id_projeto") Long idProjeto,
            @RequestBody ProjetoEdit projetoEdit) {
        ProjetoEntity projetoEntity = DataMapper.INSTANCE.projetoEditParaProjetoEntity(projetoEdit);
        ProjetoEntity projetoEntitySaved = projetoService.editProjeto(idOrganizacao, idProjeto, projetoEntity);
        ProjetoRequest projetoRequestDb = DataMapper.INSTANCE.projetoEntityParaProjetoRequest(projetoEntitySaved);
        return ResponseEntity.ok().body(projetoRequestDb);
    }

    @DeleteMapping("/{id_organizacao}/projeto/{id_projeto}")
    public ResponseEntity deleteOrganizacao(@Valid
                                            @NotNull(message = "O id da organização não pode ser nulo.")
                                            @Min(value = 1, message = "O id da organização deve ser um número positivo.")
                                            @PathVariable("id_organizacao") Long idOrganizacao,
                                            @Valid
                                            @NotNull(message = "O id do projeto não pode ser nulo.")
                                            @Min(value = 1, message = "O id do projeto deve ser um número positivo.")
                                            @PathVariable("id_projeto") Long idProjeto) {
        projetoService.deleteProjeto(idOrganizacao, idProjeto);
        return ResponseEntity.noContent().build();
    }
}
