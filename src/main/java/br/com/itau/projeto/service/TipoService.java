package br.com.itau.projeto.service;

import br.com.itau.projeto.entity.TipoEntity;
import br.com.itau.projeto.repository.TipoRepository;
import br.com.itau.util.exception.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class TipoService {

    private final TipoRepository tipoRepository;

    public TipoService(TipoRepository tipoRepository) {
        this.tipoRepository = tipoRepository;
    }

    public List<TipoEntity> getAllTipos() {
        Iterable<TipoEntity> tipoIterable = tipoRepository.findAll();
        return StreamSupport.stream(tipoIterable.spliterator(), false).collect(Collectors.toList());
    }

    public TipoEntity getById(Long id) {
        return tipoRepository.findById(id).orElseThrow(() -> new NotFoundException("O tipo informado não foi encontrado"));
    }

    public TipoEntity saveTipo(TipoEntity tipoEntity) {
        return tipoRepository.save(tipoEntity);
    }

    public TipoEntity editTipo(Long id, TipoEntity tipoEntity) {
        TipoEntity tipoEntityTarget = getById(id);

        if (tipoEntity.getNome() != null) {
            tipoEntityTarget.setNome(tipoEntity.getNome());
        }
        if (tipoEntity.getDescricao() != null) {
            tipoEntityTarget.setDescricao(tipoEntity.getDescricao());
        }

        return tipoRepository.save(tipoEntityTarget);
    }
}
