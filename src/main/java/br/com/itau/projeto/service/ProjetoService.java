package br.com.itau.projeto.service;

import br.com.itau.projeto.client.OrganizacaoClient;
import br.com.itau.projeto.entity.ProjetoEntity;
import br.com.itau.projeto.entity.builder.ProjetoEntityBuilder;
import br.com.itau.projeto.repository.ProjetoRepository;
import br.com.itau.util.exception.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProjetoService {

    private final OrganizacaoClient organizacaoClient;
    private final ProjetoRepository projetoRepository;

    public ProjetoService(OrganizacaoClient organizacaoClient, ProjetoRepository projetoRepository) {
        this.organizacaoClient = organizacaoClient;
        this.projetoRepository = projetoRepository;
    }

    public List<ProjetoEntity> findByIdOrganizacao(Long idOrganizacao) {
        return projetoRepository.findByIdOrganizacao(idOrganizacao);
    }

    public List<ProjetoEntity> findByIdOrganizacaoAndTipo(Long idOrganizacao, Long tipo) {
        return projetoRepository.findByIdOrganizacaoAndTipo(idOrganizacao, tipo);
    }

    public ProjetoEntity findById(Long idOrganizacao, Long idProjeto) {
        validaOrganizacao(idOrganizacao);
        return getProjetoEntityDb(idProjeto);
    }

    public ProjetoEntity save(Long idOrganizacao, ProjetoEntity projetoEntity) {
        validaOrganizacao(idOrganizacao);
        ProjetoEntity buildedProjetoEntity = ProjetoEntityBuilder.aProjetoEntity().descricao(projetoEntity.getDescricao())
                .idOrganizacao(idOrganizacao).nome(projetoEntity.getNome()).tipo(projetoEntity.getTipo()).build();
        return projetoRepository.save(buildedProjetoEntity);
    }

    public ProjetoEntity editProjeto(Long idOrganizacao, Long idProjeto, ProjetoEntity projetoEntity) {
        validaOrganizacao(idOrganizacao);
        ProjetoEntity projetoEntityDb = getProjetoEntityDb(idProjeto);

        if (projetoEntity.getDescricao() != null) {
            projetoEntityDb.setDescricao(projetoEntity.getDescricao());
        }
        if (projetoEntity.getNome() != null) {
            projetoEntityDb.setNome(projetoEntity.getNome());
        }
        if (projetoEntity.getTipo() != null) {
            projetoEntityDb.setTipo(projetoEntity.getTipo());
        }
        return projetoRepository.save(projetoEntityDb);
    }

    public void deleteProjeto(Long idOrganizacao, Long idProjeto) {
        validaOrganizacao(idOrganizacao);
        getProjetoEntityDb(idProjeto);
        projetoRepository.deleteById(idProjeto);
    }

    private ProjetoEntity getProjetoEntityDb(Long idProjeto) {
        return projetoRepository.findById(idProjeto).orElseThrow(() -> new NotFoundException("Projeto não encontrado."));
    }

    private void validaOrganizacao(Long idOrganizacao) {
        organizacaoClient.getOrganizacao(idOrganizacao);
    }
}
